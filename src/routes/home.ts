import express from 'express';

const router = express.Router();

router.get('/', (req, res) => {
  res.send('Hello, Boomy the Cat !'); // <-- message modifié
});

export default router;
